﻿using System;
using System.Collections.Generic;

namespace MythicTable.TestUtils.TextParsing
{
    public class Helper
    {
        public static Action<byte[]> GetRNG(params uint[] values)
        {
            return GetRNG((IEnumerable<uint>)values);
        }

        public static Action<byte[]> GetRNG(IEnumerable<uint> values)
        {
            var enumerator = values.GetEnumerator();

            void GetRandomBytes(byte[] arr)
            {
                enumerator.MoveNext();
                BitConverter.GetBytes(enumerator.Current).CopyTo(arr, 0);
            }

            return GetRandomBytes;
        }

        public static IEnumerable<uint> Roll1()
        {
            while (true)
            {
                yield return 0;
            }
        }
    }
}
