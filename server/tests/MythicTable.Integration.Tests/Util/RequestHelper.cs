using System.IO;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Newtonsoft.Json;

namespace MythicTable.Integration.TestUtils.Util
{
    public class RequestHelper
    {
        public static async Task<HttpResponseMessage> GetStreamAsync(HttpClient client, string url, CancellationToken cancellationToken = new CancellationToken())
        {
            return await MakeRequest(client, url, null, HttpMethod.Get, cancellationToken);
        }

        public static async Task<HttpResponseMessage> PostStreamAsync(HttpClient client, string url, object content, CancellationToken cancellationToken = new CancellationToken())
        {
            return await MakeRequest(client, url, content, HttpMethod.Post, cancellationToken);
        }

        public static async Task<HttpResponseMessage> PutStreamAsync(HttpClient client, string url, object content, CancellationToken cancellationToken = new CancellationToken())
        {
            return await MakeRequest(client, url, content, HttpMethod.Put, cancellationToken);
        }
        public static async Task<HttpResponseMessage> DeleteStreamAsync(HttpClient client, string url, CancellationToken cancellationToken = new CancellationToken())
        {
            return await MakeRequest(client, url, null, HttpMethod.Delete, cancellationToken);
        }

        private static async Task<HttpResponseMessage> MakeRequest(HttpClient client, string url, object content, HttpMethod method, CancellationToken cancellationToken)
        {
            using var request = new HttpRequestMessage(method, url);
            if (content == null)
            {
                return await client
                    .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken)
                    .ConfigureAwait(false);
            }
            else
            {
                using var httpContent = CreateHttpContent(content);
                request.Content = httpContent;

                return await client
                    .SendAsync(request, HttpCompletionOption.ResponseHeadersRead, cancellationToken)
                    .ConfigureAwait(false);
            }
        }

        private static HttpContent CreateHttpContent(object content)
        {
            HttpContent httpContent = null;

            if (content != null)
            {
                var ms = new MemoryStream();
                SerializeJsonIntoStream(content, ms);
                ms.Seek(0, SeekOrigin.Begin);
                httpContent = new StreamContent(ms);
                httpContent.Headers.ContentType = new MediaTypeHeaderValue("application/json");
            }

            return httpContent;
        }

        private static void SerializeJsonIntoStream(object value, Stream stream)
        {
            using (var sw = new StreamWriter(stream, new UTF8Encoding(false), 1024, true))
            using (var jtw = new JsonTextWriter(sw) { Formatting = Formatting.None })
            {
                var js = new JsonSerializer();
                js.Serialize(jtw, value);
                jtw.Flush();
            }
        }
    }
}