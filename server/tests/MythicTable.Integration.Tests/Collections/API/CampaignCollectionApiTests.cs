using System.Collections.Generic;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MythicTable.Campaign.Data;
using MythicTable.Integration.TestUtils.Util;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using Xunit;

namespace MythicTable.Integration.Tests.Collections.API
{
    public class CampaignCollectionApiTests
    {
        private readonly HttpClient client;
        private readonly TestServer server;
        private const string collectionName = "collection01";
        private const string campaignId = "campaign01";

        public CampaignCollectionApiTests()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            server = new TestServer(builder);
            client = server.CreateClient();
        }

        [Fact]
        public async Task RequiresCampaign()
        {
            using var response = await RequestHelper.GetStreamAsync(client, $"/api/collections/{collectionName}/campaign/{campaignId}");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetCollectionReturnsEmpty()
        {
            await ProfileTestUtil.Login(client);

            var campaign = await CreateCampaign(new CampaignDTO());

            List<JObject> jObjects = await GetCollection(campaign);
            Assert.Empty(jObjects);
        }

        [Fact]
        public async Task CollectionsAreCampaignSpecific()
        {
            await ProfileTestUtil.Login(client);

            var campaign1 = await CreateCampaign(new CampaignDTO());
            var campaign2 = await CreateCampaign(new CampaignDTO());

            await CreateCampaignObject(campaign1, new JObject());

            Assert.Single(await GetCollection(campaign1));
            Assert.Empty(await GetCollection(campaign2));
        }

        [Fact]
        public async Task CampaignCollectionsAreNotUserSpecific()
        {
            await ProfileTestUtil.Login(client);

            var campaign = await CreateCampaign(new CampaignDTO());

            await CreateCampaignObject(campaign, new JObject());

            Assert.Single(await GetCollection(campaign));


            var request = server.CreateRequest($"/api/collections/{collectionName}/campaign/{campaign.Id}");
            request.AddHeader(TestStartup.FAKE_USER_ID_HEADER, "fake-user-02");
            var response = await request.GetAsync();
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            var collections = JsonConvert.DeserializeObject<List<JObject>>(json);

            Assert.Single(collections);
        }

        private async Task<CampaignDTO> CreateCampaign(CampaignDTO campaign)
        {
            using var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<CampaignDTO>(json);
        }

        private async Task<List<JObject>> GetCollection(CampaignDTO campaign)
        {
            using var response = await RequestHelper.GetStreamAsync(client, $"/api/collections/{collectionName}/campaign/{campaign.Id}");
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<List<JObject>>(json);
        }

        private async Task<JObject> CreateCampaignObject(CampaignDTO campaign, JObject jObject)
        {
            using var response = await RequestHelper.PostStreamAsync(client, $"/api/collections/{collectionName}/campaign/{campaign.Id}", jObject);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<JObject>(json);
        }
    }
}
