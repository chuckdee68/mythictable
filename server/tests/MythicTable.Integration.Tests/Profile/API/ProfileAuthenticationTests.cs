using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using MythicTable.Integration.TestUtils.Util;
using MythicTable.Profile.Data;
using Xunit;

namespace MythicTable.Integration.Tests.Profile.API
{
    public class ProfileAuthenticationTests : IClassFixture<WebApplicationFactory<MythicTable.Startup>>
    {
        private readonly WebApplicationFactory<MythicTable.Startup> _factory;

        public ProfileAuthenticationTests(WebApplicationFactory<MythicTable.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task GetDoesNotRequireAuth()
        {
            var client = _factory.CreateClient();
            using var response = await RequestHelper.GetStreamAsync(client, "/api/profiles/some_user");
            Assert.Equal(HttpStatusCode.NotFound, response.StatusCode);
        }

        [Fact]
        public async Task GetManyDoesNotRequireAuth()
        {
            var client = _factory.CreateClient();
            using var response = await RequestHelper.GetStreamAsync(client, "/api/profiles?userId=1&userId=2");
            response.EnsureSuccessStatusCode();
        }

        [Fact]
        public async Task GetMeRequiresAuth()
        {
            var client = _factory.CreateClient();
            using var response = await RequestHelper.GetStreamAsync(client, "/api/profiles/me");
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }

        [Fact]
        public async Task PutRequiresAuth()
        {
            var client = _factory.CreateClient();
            using var response = await RequestHelper.PutStreamAsync(client, "/api/profiles", new ProfileDto());
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
    }
}