using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.TestHost;
using MythicTable.Campaign.Data;
using MythicTable.Integration.TestUtils.Util;
using MythicTable.TestUtils.Profile.Util;
using Newtonsoft.Json;
using Xunit;

namespace MythicTable.Integration.Tests.Campaign
{
    public class CampaignTests
    {
        [Fact]
        public async Task CreateCampaignTest()
        {
            var builder = new WebHostBuilder().UseStartup<TestStartup>();
            var server = new TestServer(builder);
            var client = server.CreateClient();

            await ProfileTestUtil.Login(client);

            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            using var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign);
            response.EnsureSuccessStatusCode();
            Assert.Equal("application/json; charset=utf-8", response.Content.Headers.ContentType.ToString());
            var json = await response.Content.ReadAsStringAsync();
            var newCampaign = JsonConvert.DeserializeObject<CampaignDTO>(json);
            Assert.Equal("Integration Test Campaign", newCampaign.Name);
        }
    }
}