using System.Net;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc.Testing;
using MythicTable.Campaign.Data;
using MythicTable.Integration.TestUtils.Util;
using Xunit;

namespace MythicTable.Integration.Tests.Campaign
{
    public class CampaignAuthenticationTests : IClassFixture<WebApplicationFactory<MythicTable.Startup>>
    {
        private readonly WebApplicationFactory<MythicTable.Startup> _factory;

        public CampaignAuthenticationTests(WebApplicationFactory<MythicTable.Startup> factory)
        {
            _factory = factory;
        }

        [Fact]
        public async Task CreateCampaignRequiresAuthenticationTest()
        {
            var client = _factory.CreateClient();

            var campaign = new CampaignDTO()
            {
                Name = "Integration Test Campaign"
            };

            using var response = await RequestHelper.PostStreamAsync(client, "/api/campaigns", campaign);
            Assert.Equal(HttpStatusCode.Unauthorized, response.StatusCode);
        }
    }
}