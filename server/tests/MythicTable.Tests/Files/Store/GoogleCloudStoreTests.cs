using Microsoft.Extensions.Configuration;
using MythicTable.Files.Store;
using System.Collections.Generic;
using Xunit;

namespace MythicTable.Tests.Files.Store
{
    public class GoogleCloudStoreTests
    {
        public GoogleCloudStoreTests()
        {
        }

        [Fact(Skip = "WIP")]
        public void TestConstructorDoesntCreateDirectory()
        {
            var config = new Dictionary<string, string>
            {
                {"GoogleCredentialFile", "none"},
                {"GoogleCloudStorageBucket", "none"}
            };

            var configuration = new ConfigurationBuilder()
                .AddInMemoryCollection(config)
                .Build();

            var gcpStore = new GoogleCloudStore(configuration);
        }
    }
}
