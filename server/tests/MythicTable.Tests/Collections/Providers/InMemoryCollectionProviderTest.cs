﻿using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Collections.Providers;
using MythicTable.Common.Exceptions;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;
using Xunit;

namespace MythicTable.Tests.Collections.Providers
{
    public class InMemoryCollectionProviderTest
    {
        private const string userId = "test-user";
        private const string collectionName = "test";

        public Mock<ILogger<InMemoryCollectionProvider>> loggerMock;
        private InMemoryCollectionProvider provider;
        public InMemoryCollectionProviderTest()
        {
            loggerMock = new Mock<ILogger<InMemoryCollectionProvider>>();
            provider = new InMemoryCollectionProvider(loggerMock.Object);
        }

        [Fact]
        public async Task GetReturnsEmptyListWhenNotPresent()
        {
            var jObjects = await provider.Get(userId, collectionName);
            Assert.Empty(jObjects);
        }

        [Fact]
        public async Task CreatesAndGetsJObject()
        {
            await provider.Create(userId, collectionName, new JObject());
            var jObjects = await provider.Get(userId, collectionName);
            Assert.Single(jObjects);
        }

        [Fact]
        public async Task CreatesGeneratesId()
        {
            var jObject = await provider.Create(userId, collectionName, new JObject());
            var jObjects = await provider.Get(userId, collectionName);
            Assert.Single(jObjects);
            Assert.NotNull(jObject.GetId());
            Assert.Equal(jObject.GetId(), jObjects[0].GetId());
        }

        [Fact]
        public async Task CreatesGeneratesUniqueId()
        {
            var jObject1 = await provider.Create(userId, collectionName, new JObject());
            var jObject2 = await provider.Create(userId, collectionName, new JObject());
            Assert.NotEqual(jObject1.GetId(), jObject2.GetId());
        }

        [Fact]
        public async Task JObjectTypesAreExcluse()
        {
            await provider.Create(userId, collectionName, new JObject());
            var jObjects = await provider.Get(userId, "other-type");
            Assert.Empty(jObjects);
        }

        [Fact]
        public async Task MaintainsTwoTypes()
        {
            await provider.Create(userId, collectionName, new JObject { { "name", "test1" } });
            await provider.Create(userId, "test2", new JObject { { "name", "test2" } });
            var jObjects = await provider.Get(userId, collectionName);
            Assert.Single(jObjects);
            Assert.Equal("test1", jObjects[0]["name"]);
            jObjects = await provider.Get(userId, "test2");
            Assert.Single(jObjects);
            Assert.Equal("test2", jObjects[0]["name"]);
        }

        [Fact]
        public async Task CanDelete()
        {
            var jObject = await provider.Create(userId, collectionName, new JObject());
            var jObjects = await provider.Get(userId, collectionName);
            Assert.Single(jObjects);
            var numDeleted = await provider.Delete(userId, collectionName, jObject.GetId());
            Assert.Equal(1, numDeleted);
            jObjects = await provider.Get(userId, collectionName);
            Assert.Empty(jObjects);
        }

        [Fact]
        public async Task DeleteFromWrongCollectionFailsToDelete()
        {
            var jObject = await provider.Create(userId, collectionName, new JObject());
            var jObjects = await provider.Get(userId, collectionName);
            Assert.Single(jObjects);
            var numDeleted = await provider.Delete(userId, "wrong-collection", jObject.GetId());
            Assert.Equal(0, numDeleted);
            jObjects = await provider.Get(userId, collectionName);
            Assert.Single(jObjects);
        }

        [Fact]
        public async Task UpdatesJObject()
        {
            var jObject = await provider.Create(userId, collectionName, new JObject());
            var jObjects = await provider.Get(userId, collectionName);

            JsonPatchDocument patch = new JsonPatchDocument().Add("foo", "bar");
            var numUpdated = await provider.Update(userId, collectionName, jObject.GetId(), patch);

            Assert.Equal(1, numUpdated);
            jObjects = await provider.Get(userId, collectionName);
            Assert.Single(jObjects);
            Assert.Equal("bar", jObjects[0]["foo"]);
        }

        [Fact]
        public async Task GetThrowsWhenNoCollectionNotFound()
        {
            var exception = await Assert.ThrowsAsync<MythicTableException>(() => provider.Get(userId, collectionName, "some item"));

            string expected = $"Could not find item 'some item' in collection '{collectionName}' for user '{userId}'";
            Assert.Equal(expected, exception.Message);
            VerifyLog(LogLevel.Error, expected);
        }

        [Fact]
        public async Task GetThrowsWhenNoItemNotFound()
        {
            var jObject = await provider.Create(userId, collectionName, new JObject());
            var exception = await Assert.ThrowsAsync<MythicTableException>(() => provider.Get(userId, collectionName, "some item"));

            string expected = $"Could not find item 'some item' in collection '{collectionName}' for user '{userId}'";
            Assert.Equal(expected, exception.Message);
            VerifyLog(LogLevel.Error, expected);
        }

        [Fact]
        public async Task FailedDeleteLogsTheFailure()
        {
            var numDeleted = await provider.Delete(userId, collectionName, "missing-item");

            Assert.Equal(0, numDeleted);
            string expected = $"Could not delete item 'missing-item' in collection '{collectionName}' for user '{userId}'";
            VerifyLog(LogLevel.Warning, expected);
        }

        [Fact]
        public async Task RecordsOwner()
        {
            var jObject = await provider.Create(userId, collectionName, new JObject());
            Assert.Equal(userId, jObject["_userid"]);
        }

        [Fact]
        public async Task FailedUpdateLogsTheFailure()
        {
            JsonPatchDocument patch = new JsonPatchDocument().Add("foo", "bar");
            var numUpdated = await provider.Update(userId, collectionName, "missing-item", patch);

            Assert.Equal(0, numUpdated);
            string expected = $"Could not update item 'missing-item' in collection '{collectionName}' for user '{userId}'";
            VerifyLog(LogLevel.Warning, expected);
        }

        private void VerifyLog(LogLevel expectedLevel, string expected)
        {
            loggerMock.Verify(l => l.Log(
                It.Is<LogLevel>(level => level == expectedLevel),
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((v, t) => v.ToString() == expected),
                It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)));
        }
    }
}
