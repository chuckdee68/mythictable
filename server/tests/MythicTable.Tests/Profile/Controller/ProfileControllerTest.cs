using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Caching.Memory;
using Microsoft.Extensions.Logging;
using Moq;
using MythicTable.Campaign.Data;
using MythicTable.Profile.Controller;
using MythicTable.Collections.Providers;
using MythicTable.Profile.Data;
using MythicTable.Profile.Exceptions;
using Xunit;

namespace MythicTable.Tests.Profile.Controller
{
    public class ProfileControllerTest : IAsyncLifetime
    {
        private ProfileController controller;
        private IProfileProvider provider;
        private ICampaignProvider campaignProvider;
        private ICollectionProvider collectionProvider;

        private string User { get; set; } = "jon@example.com";

        public Task InitializeAsync()
        {
            provider = new InMemoryProfileProvider(Mock.Of<ILogger<InMemoryProfileProvider>>());
            campaignProvider = new InMemoryCampaignProvider();
            collectionProvider = new InMemoryCollectionProvider(Mock.Of<ILogger<InMemoryCollectionProvider>>());
            controller = new ProfileController(provider, campaignProvider, collectionProvider, new MemoryCache(new MemoryCacheOptions()));

            var mockHttpContext = new Mock<HttpContext>();
            mockHttpContext.Setup(hc => hc.User.FindFirst(It.IsAny<string>()))
                           .Returns(() => new Claim("", User));
            controller.ControllerContext.HttpContext = mockHttpContext.Object;
            return Task.CompletedTask;
        }

        public Task DisposeAsync()
        {
            return Task.CompletedTask;
        }

        [Fact]
        public async void MeCreatesProfile()
        {
            var profile = await controller.Me();

            var results = await controller.Get(profile.Id);
            Assert.Equal(profile.Id, results.Id);
        }

        [Fact]
        public async void BasicFlow()
        {
            var result = await controller.Me();

            result.ImageUrl = "http://example.com/test.png";
            await controller.Put(result);

            var allProfiles = await controller.Get(new List<string>{ result.Id });
            Assert.Single(allProfiles);
        }

        [Fact]
        public async void MePopulatesUserId()
        {
            var result = await controller.Me();
            Assert.Equal("jon", result.DisplayName);
        }

        [Fact]
        public async void MePopulatesDisplayName()
        {
            var result = await controller.Me();
            Assert.Equal(User, result.UserId);
        }

        [Fact]
        public async void UpdateRequiresSameUserIs()
        {
            var profile = new ProfileDto
            {
                Id = "Invalid",
                DisplayName = "Test"
            };
            var result = await controller.Me();
            var exception = await Assert.ThrowsAsync<ProfileNotAuthorizedException>(() => controller.Put(profile));
            Assert.Equal($"User: '{result.Id}' is not authorized to update profile for user: '{profile.Id}'", exception.Message);
        }
    }
}