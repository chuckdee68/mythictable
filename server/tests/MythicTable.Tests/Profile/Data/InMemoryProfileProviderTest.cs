﻿using System;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;
using MongoDB.Bson;
using Moq;
using MythicTable.Profile.Data;
using MythicTable.Profile.Exceptions;
using Xunit;

namespace MythicTable.Tests.Profile.Data
{
    public class InMemoryProfileProviderTest
    {
        private const string UserId = "test-user";
        private const string SecondUserId = "other-user";
        
        protected InMemoryProfileProvider Provider { get; }
        protected Mock<ILogger<InMemoryProfileProvider>> LoggerMock { get; }

        public InMemoryProfileProviderTest()
        {
            LoggerMock = new Mock<ILogger<InMemoryProfileProvider>>();
            Provider = new InMemoryProfileProvider(LoggerMock.Object);
        }


        [Fact]
        public async Task CreatesAssignsId()
        {
            var profile = await Provider.Create(new ProfileDto(), UserId);
            Assert.NotNull(profile.Id);
            Assert.Equal(UserId, profile.UserId);
        }

        [Fact]
        public async Task CreatesGeneratesUniqueId()
        {
            var profile1 = await Provider.Create(new ProfileDto(), UserId);
            var profile2 = await Provider.Create(new ProfileDto(), SecondUserId);
            Assert.NotEqual(profile1.Id, profile2.Id);
        }

        [Fact]
        public async Task CanDelete()
        {
            var profile = await Provider.Create(new ProfileDto(), UserId);
            await Provider.Delete(profile.Id);
            await Assert.ThrowsAsync<ProfileNotFoundException>(() => Provider.Get(profile.Id));
        }

        [Fact]
        public async Task GetThrowsWhenProfileNotFound()
        {
            var id = ObjectId.GenerateNewId().ToString();
            var exception = await Assert.ThrowsAsync<ProfileNotFoundException>(() => Provider.Get(id));
            Assert.Equal($"Cannot find user: {id}", exception.Message);
            VerifyLog(LogLevel.Error, $"Cannot find user: {id}");
        }

        [Fact]
        public async Task FailedDeleteLogsTheFailure()
        {
            var id = ObjectId.GenerateNewId().ToString();
            var exception = await Assert.ThrowsAsync<ProfileNotFoundException>(() => Provider.Delete(id));
            Assert.Equal($"Profile for user: {id} doesn't exist", exception.Message);
            VerifyLog(LogLevel.Error, $"Cannot find user: {id}");
        }

        [Fact]
        public async Task FailedUpdateLogsTheFailure()
        {
            var exception = await Assert.ThrowsAsync<ProfileInvalidException>(() => Provider.Update(null));
            Assert.Equal("The profile is null", exception.Message);
            VerifyLog(LogLevel.Error, "The profile is null");
        }

        [Fact]
        public async Task UpdateRequiresProfileWithUserId()
        {
            var exception = await Assert.ThrowsAsync<ProfileInvalidException>(() => Provider.Update(new ProfileDto()));
            Assert.Equal("The profile MUST have an id", exception.Message);
            VerifyLog(LogLevel.Error, "The profile MUST have an id");
        }

        private void VerifyLog(LogLevel expectedLevel, string expected)
        {
            LoggerMock.Verify(l => l.Log(
                It.Is<LogLevel>(level => level == expectedLevel),
                It.IsAny<EventId>(),
                It.Is<It.IsAnyType>((v, t) => v.ToString() == expected),
                It.IsAny<Exception>(),
                It.Is<Func<It.IsAnyType, Exception, string>>((v, t) => true)));
        }
    }
}
