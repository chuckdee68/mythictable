﻿using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.JsonPatch;
using Microsoft.Extensions.Logging;
using MythicTable.Common.Exceptions;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace MythicTable.Collections.Providers
{
    public class InMemoryCollectionProvider : ICollectionProvider
    {
        private const string Id = "_id";
        private const string UserId = "_userid";
        private const string Collection = "_collection";
        private const string Campaign = "_campaign";

        private readonly List<JObject> items = new List<JObject>();
        private readonly ILogger<InMemoryCollectionProvider> logger;

        private long lastId;

        public InMemoryCollectionProvider(ILogger<InMemoryCollectionProvider> logger)
        {
            this.logger = logger;
        }

        public Task<JObject> Create(string userId, string collection, JObject jObject)
        {
            jObject[Id] = (++lastId).ToString();
            jObject[UserId] = userId;
            jObject[Collection] = collection;
            items.Add(jObject);
            return Task.FromResult(jObject);
        }

        public Task<List<JObject>> Get(string userId, string collection)
        {
            var results = items.Where(i => i[UserId]?.ToString() == userId
                                           && i[Collection]?.ToString() == collection).ToList();
            return Task.FromResult(results);
        }

        public Task<JObject> Get(string userId, string collection, string id)
        {
            var found = items.FirstOrDefault(i => i[UserId]?.ToString() == userId
                                                  && i[Id]?.ToString() == id &&
                                                  i[Collection]?.ToString() == collection);
            if (found == null)
            {
                throw Exception($"Could not find item '{id}' in collection '{collection}' for user '{userId}'");
            }
            return Task.FromResult(found);
        }

        public Task<int> Delete(string userId, string collection, string id)
        {
            var removed = items.RemoveAll(i =>
                i[UserId]?.ToString() == userId && i[Id]?.ToString() == id && i[Collection]?.ToString() == collection);
            if (removed == 0)
            {
                logger.LogWarning($"Could not delete item '{id}' in collection '{collection}' for user '{userId}'");
            }
            return Task.FromResult(removed);
        }

        public Task<int> Update(string userId, string collection, string id, JsonPatchDocument patch)
        {
            var found = items.FirstOrDefault(i =>
                i[UserId]?.ToString() == userId && i[Id]?.ToString() == id && i[Collection]?.ToString() == collection);
            if (found != null)
            {
                foreach (var obj in patch.Operations.Select(operation =>
                    JsonConvert.DeserializeObject<ExpandoObject>(found.ToString())))
                {
                    patch.ApplyTo(obj);
                    found = JObject.Parse(JsonConvert.SerializeObject(obj));
                }

                items.RemoveAll(jObject => jObject[Id]?.ToString() == id);
                items.Add(found);
                return Task.FromResult(1);
            }

            logger.LogWarning($"Could not update item '{id}' in collection '{collection}' for user '{userId}'");
            return Task.FromResult(0);
        }

        public Task<JObject> CreateByCampaign(string userId, string collection, string campaignId, JObject jObject)
        {
            jObject[Id] = (++lastId).ToString();
            jObject[UserId] = userId;
            jObject[Collection] = collection;
            jObject[Campaign] = campaignId;
            items.Add(jObject);
            return Task.FromResult(jObject);
        }

        public Task<List<JObject>> GetByCampaign(string collection, string campaignId)
        {
            return Task.FromResult(items
                .Where(i => i[Campaign]?.ToString() == campaignId && i[Collection]?.ToString() == collection).ToList());
        }

        public Task<JObject> GetByCampaign(string collection, string campaignId, string id)
        {
            var found = items.FirstOrDefault(i => i[Campaign]?.ToString() == campaignId
                                                  && i[Id]?.ToString() == id &&
                                                  i[Collection]?.ToString() == collection);
            if (found == null)
            {
                throw Exception($"Could not find campaign '{campaignId}' in collections'");
            }
            return Task.FromResult(found);
        }

        public Task<int> UpdateByCampaign(string collection, string campaignId, string id, JsonPatchDocument patch)
        {
            var found = items.FirstOrDefault(i =>
                i[Campaign]?.ToString() == campaignId && i[Id]?.ToString() == id &&
                i[Collection]?.ToString() == collection);
            if (found != null)
            {
                foreach (var obj in patch.Operations.Select(operation =>
                    JsonConvert.DeserializeObject<ExpandoObject>(found.ToString())))
                {
                    patch.ApplyTo(obj);
                    found = JObject.Parse(JsonConvert.SerializeObject(obj));
                }

                items.RemoveAll(jObject => jObject[Id]?.ToString() == id);
                items.Add(found);
                return Task.FromResult(1);
            }

            logger.LogWarning($"Could not update item '{id}' in collection '{collection}' for campaign '{campaignId}'");
            return Task.FromResult(0);
        }

        private MythicTableException Exception(string message)
        {
            logger.LogError(message);
            return new MythicTableException(message);
        }
    }
}