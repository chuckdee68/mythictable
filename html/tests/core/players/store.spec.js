import { getters, actions } from '@/core/collections/players/store';

describe('PlayerStore', () => {
    describe('getters', () => {
        const { getPlayer, getPlayers } = getters;
        const rootGetters = {
            'collections/getItem': jest.fn(),
            'collections/getCollection': jest.fn(),
        };

        afterEach(() => {
            rootGetters['collections/getItem'].mockClear();
            rootGetters['collections/getCollection'].mockClear();
        });

        describe('getPlayer', () => {
            it('invokes getItem by players in collection', () => {
                getPlayer(null, null, null, rootGetters)('test-player');
                expect(rootGetters['collections/getItem']).toHaveBeenCalledWith('players', 'test-player');
            });
        });

        describe('getPlayers', () => {
            it('invokes getCollection by players in collection', () => {
                getPlayers(null, null, null, rootGetters)();
                expect(rootGetters['collections/getCollection']).toHaveBeenCalledWith('players');
            });
        });
    });

    describe('actions', () => {
        const { add, update, load } = actions;

        describe('add', () => {
            it('dispatches add to players in collection', () => {
                const dispatch = jest.fn();
                add({ dispatch }, { player: { name: 'test-player' } });
                expect(dispatch).toHaveBeenCalledWith(
                    'collections/add',
                    { collection: 'players', item: { name: 'test-player' } },
                    { root: true },
                );
            });
        });

        describe('update', () => {
            it('dispatches update to players in collection', () => {
                const dispatch = jest.fn();
                const patch = [{ op: 'replace', path: '/foo', value: 'bar' }];
                update({ dispatch }, { id: 'test-player', patch });
                expect(dispatch).toHaveBeenCalledWith(
                    'collections/update',
                    { collection: 'players', id: 'test-player', patch },
                    { root: true },
                );
            });
        });

        describe('load', () => {
            it('dispatches load to players in collection', () => {
                const dispatch = jest.fn();
                load({ dispatch });
                expect(dispatch).toHaveBeenCalledWith('collections/load', { collection: 'players' }, { root: true });
            });
        });
    });
});
