import CharacterItem from '@/characters/components/character-library/CharacterItem.vue';
import CharacterList from '@/characters/components/character-library/CharacterList.vue';
import { mount } from '@vue/test-utils';

describe('CharacterList', () => {
    it('should render without an error', () => {
        const wrapper = mount(CharacterList);
        expect(wrapper.contains('div')).toBeTruthy();
    });
    it('should not contain characters if passed no items', () => {
        const wrapper = mount(CharacterList);
        expect(wrapper.contains(CharacterItem)).toBeFalsy();
    });
    it('should contain characters if passed items', () => {
        const wrapper = mount(CharacterList, {
            propsData: {
                characters: [
                    {
                        _id: 'id',
                        name: 'name',
                        image: 'image',
                    },
                ],
            },
        });
        expect(wrapper.contains(CharacterItem)).toBeTruthy();
    });
});
