import CharacterEditor from '@/characters/components/character-editor/CharacterEditor.vue';
import { mount } from '@vue/test-utils';

describe('CharacterEditor', () => {
    it('should not render without original getter', () => {
        const wrapper = mount(CharacterEditor, {
            computed: {
                original: () => ({}),
            },
        });
        expect(wrapper.contains('div')).toBeFalsy();
    });
    it('should render with original getter', () => {
        const wrapper = mount(CharacterEditor, {
            computed: {
                original: () => ({
                    _id: 'foo',
                    image: '/image.png',
                    borderMode: 'coin',
                    borderColor: 'green',
                }),
            },
        });
        // Trigger watcher
        wrapper.vm.$options.watch.original.call(wrapper.vm);
        expect(wrapper.contains('div')).toBeTruthy();
    });
});
