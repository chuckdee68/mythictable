import CharacterTokenEditor from '@/characters/components/character-editor/CharacterTokenEditor.vue';
import { shallowMount } from '@vue/test-utils';

describe('CharacterTokenEditor', () => {
    it('should render without errors', () => {
        const wrapper = shallowMount(CharacterTokenEditor, {
            propsData: {
                value: {
                    borderMode: 'coin',
                    borderColor: '#ffffff',
                    tokenSize: '2',
                },
            },
        });

        expect(wrapper.find('div').exists()).toBeTruthy();
    });
    it('should render selections for given props', () => {
        const wrapper = shallowMount(CharacterTokenEditor, {
            propsData: {
                value: {
                    borderMode: 'coin',
                    borderColor: '#ffffff',
                    tokenSize: '2',
                },
            },
        });

        expect(wrapper.find('.mode-selector .active').text()).toMatch('Coin');
        expect(wrapper.find('.color-selector .active').element.style.backgroundColor).toMatch('rgb(255, 255, 255)');
        expect(wrapper.find('.sizeSelector .active').text()).toMatch('Medium');
    });
    it('should render selections for different props', () => {
        const wrapper = shallowMount(CharacterTokenEditor, {
            propsData: {
                value: {
                    borderMode: 'square',
                    borderColor: '#000000',
                    tokenSize: '3',
                },
            },
        });

        expect(wrapper.find('.mode-selector .active').text()).toMatch('Square');
        expect(wrapper.find('.color-selector .active').element.style.backgroundColor).toMatch('rgb(0, 0, 0)');
        expect(wrapper.find('.sizeSelector .active').text()).toMatch('Large');
    });
});
