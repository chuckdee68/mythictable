import CharacterDescriptionEditor from '@/characters/components/character-editor/CharacterDescriptionEditor.vue';
import { shallowMount } from '@vue/test-utils';

describe('CharacterDescriptionEditor', () => {
    it('should render without errors', () => {
        const wrapper = shallowMount(CharacterDescriptionEditor, {
            propsData: {
                value: {
                    name: 'foo',
                    description: 'bar',
                },
            },
        });

        expect(wrapper.find('div').exists()).toBeTruthy();
    });
    it('should render content for given props', () => {
        const wrapper = shallowMount(CharacterDescriptionEditor, {
            propsData: {
                value: {
                    name: 'foo',
                    description: 'bar',
                },
            },
        });

        expect(wrapper.find('.details input').element.value).toMatch('foo');
        expect(wrapper.find('.expand textarea').element.value).toMatch('bar');
    });
});
