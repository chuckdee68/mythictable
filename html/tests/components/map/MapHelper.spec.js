import getAllImages from '@/maps/components/MapHelper.js';

const maps = [
    {
        _id: 'debug',
        map: { stage: '.' },
        stage: {
            grid: { type: 'square', size: 50 },
            bounds: {
                nw: { q: -1, r: -1 },
                se: { q: 29, r: 20 },
            },
            color: '#223344',
            elements: [
                {
                    id: 'background',
                    asset: { kind: 'image', src: '/static/assets/hideout.png' },
                    pos: { q: 0, r: 0, pa: '00' },
                },
            ],
        },
    },
];

const expectedImages = {
    debug: {
        id: 'background',
        asset: { kind: 'image', src: '/static/assets/hideout.png' },
    },
};

describe('GameMat component', () => {
    describe('map conversion', () => {
        it('should pull images from map description', () => {
            const images = getAllImages(maps);
            expect(images).toEqual(expectedImages);
        });
    });
});
