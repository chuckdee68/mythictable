import Chat from '@/chat/components/Chat.vue';
import ChatInput from '@/chat/components/ChatInput.vue';
import ChatItem from '@/chat/components/ChatItem.vue';
import { shallowMount } from '@vue/test-utils';

describe('Chat', () => {
    it('should render without an error', () => {
        const wrapper = shallowMount(Chat, {
            computed: {
                getImage: () => '',
                rollLog: () => [], // Should not be mocking this, but I can't figure out how to test it without rewriting the component
            },
        });
        expect(wrapper.contains(ChatInput)).toBeTruthy();
    });
    it('should render chat items', () => {
        const wrapper = shallowMount(Chat, {
            computed: {
                getImage: () => '',
                rollLog: () => [
                    {
                        id: 'id',
                        displayName: 'displayName',
                        result: { message: '', elements: [], dice: [] },
                        userId: 'userId',
                    },
                ], // Should not be mocking this, but I can't figure out how to test it without rewriting the component
            },
        });
        expect(wrapper.contains(ChatItem)).toBeTruthy();
    });
});
