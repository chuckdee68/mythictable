import { addCampaign, getCampaigns } from '@/core/api/files/files';

const CampaignStore = {
    namespaced: true,
    state: {
        campaigns: [],
    },
    mutations: {
        updateCampaigns(state, campaigns) {
            state.campaigns = [...campaigns];
        },
    },
    actions: {
        async addCampaign({ dispatch }, event) {
            let files;
            files = event && event.target && event.target.files;
            await addCampaign(files)
                .then(() => {
                    dispatch('getCampaigns');
                })
                .catch(err => {
                    console.error(err);
                });
        },
        async getCampaigns({ commit }) {
            await getCampaigns().then(result => {
                commit('updateCampaigns', result);
            });
        },
    },
    getters: {
        campaigns: state => {
            return state.campaigns;
        },
    },
};

export default CampaignStore;
