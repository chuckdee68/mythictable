export default {
    name: 'New Map',
    map: { stage: '.' },
    stage: {
        grid: { type: 'square', size: 50, thickness: 2, color: '#000', lineFullness: 0.35 },
        bounds: {
            nw: { q: 0, r: 0 },
            se: { q: 20, r: 20 },
        },
        elements: [
            {
                id: 'background',
                asset: { kind: 'image', src: '/static/assets/hideout.png' },
                pos: { q: 0, r: 0, pa: '00' },
            },
        ],
    },
};
