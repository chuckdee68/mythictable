import { COLLECTION_TYPES } from '@/core/collections/constants';
import * as jsonpatch from 'fast-json-patch';
import { Token } from './model';

export const getters = {
    getRawToken: ({ rootGetters }) => id => {
        return rootGetters['collections/getItem'](COLLECTION_TYPES.tokens, id);
    },
    getToken: ({ rootGetters }) => id => {
        return new Token(rootGetters['collections/getItem'](COLLECTION_TYPES.tokens, id));
    },
    getTokens: (state, getters, rootState, rootGetters) => {
        return Object.values(rootGetters['collections/getCollection'](COLLECTION_TYPES.tokens)).map(
            token => new Token(token),
        );
    },
    getTokensByMapId: (state, getters, rootState, rootGetters) => mapId => {
        return Object.values(rootGetters['collections/getCollection'](COLLECTION_TYPES.tokens))
            .map(token => new Token(token))
            .filter(token => token.mapId === mapId);
    },
};

export const actions = {
    async spawn({ dispatch }, token) {
        return await dispatch(
            'collections/add',
            { collection: COLLECTION_TYPES.tokens, item: new Token(token) },
            { root: true },
        );
    },
    /**
     *
     * @param {Vuex.context} param0
     * @param {Token} param1
     */
    async remove({ dispatch }, { id }) {
        return await dispatch('collections/remove', { collection: COLLECTION_TYPES.tokens, id }, { root: true });
    },
    /**
     *
     * @param {Vuex.context} context
     * @param {Token} editedToken
     */
    async update(context, editedToken) {
        const currentToken = getters.getRawToken(context)(editedToken._id);
        if (!currentToken) throw new Error(`Could not find Token to update with id '${editedToken._id}'`);

        // Position is changed by moveToken, not update
        const changes = {
            ...editedToken,
            pos: currentToken.pos,
        };
        const patch = jsonpatch.compare(currentToken, new Token(changes));

        return await context.dispatch(
            'collections/update',
            { collection: COLLECTION_TYPES.tokens, id: editedToken._id, patch },
            { root: true },
        );
    },
    /**
     *
     * @param {Vuex.context} context
     * @param {Token} movedToken
     */
    async moveToken(context, movedToken) {
        console.log('moved', movedToken);
        const currentToken = new Token(
            context.rootGetters['collections/getItem'](COLLECTION_TYPES.tokens, movedToken._id),
        );
        if (!currentToken) throw new Error(`Could not find Token to update with id '${movedToken.id}'`);
        const patch = jsonpatch.compare({ pos: currentToken.pos }, { pos: movedToken.pos });

        return await context.dispatch(
            'collections/update',
            { collection: COLLECTION_TYPES.tokens, id: movedToken._id, patch },
            { root: true },
        );
    },
    async load({ dispatch }) {
        console.log('Loading tokens...');
        return await dispatch('collections/load', { collection: COLLECTION_TYPES.tokens }, { root: true });
    },
};

const TokenStore = {
    namespaced: true,
    getters,
    actions,
};

export default TokenStore;
