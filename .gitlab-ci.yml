variables:
    # When using dind service we need to instruct docker, to talk with the
    # daemon started inside of the service. The daemon is available with
    # a network connection instead of the default /var/run/docker.sock socket.
    #
    # The 'docker' hostname is the alias of the service container as described at
    # https://docs.gitlab.com/ee/ci/docker/using_docker_images.html#accessing-the-services
    #
    # Note that if you're using Kubernetes executor, the variable should be set to
    # tcp://localhost:2375 because of how Kubernetes executor connects services
    # to the job container
    DOCKER_HOST: tcp://docker:2375/
    # When using dind, it's wise to use the overlayfs driver for
    # improved performance.
    DOCKER_DRIVER: overlay2
    CONTAINER_IMAGE: $CI_REGISTRY_IMAGE
    CONTAINER_TAG: $CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA
    DOCKER_IMAGE: $CI_REGISTRY_IMAGE:$CI_PIPELINE_IID-$CI_COMMIT_SHORT_SHA
    DOCKER_ASPNETCORE_IMAGE: mcr.microsoft.com/dotnet/core/sdk:3.1.102-bionic
    MIGRATOR_CONTAINER_IMAGE: $CI_REGISTRY_IMAGE/migrator

stages:
    - build
    - test
    - package
    - containerize
    - container_scanning
    - deploy

build:client:
    image: node:lts-alpine
    stage: build
    except:
        - releases/firstplayable
    script:
        - cd html
        - npm install
        - npm run build
    artifacts:
        name: "client-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - html/dist/
        expire_in: 1h
    cache:
        key: "npm-2020-10-31"
        paths:
            - html/node_modules/

build:server:
    image: $DOCKER_ASPNETCORE_IMAGE
    stage: build
    script:
        - dotnet restore --packages .nuget
        - dotnet build -c Release --no-restore --packages .nuget
    artifacts:
        name: "server-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - server/src/*/bin
            - server/src/MythicTable/obj/project.assets.json
            - server/tests/*/bin
        expire_in: 1h
    cache:
        key: nuget
        paths:
            - .nuget/

test:client:
    stage: test
    image: node:lts-alpine
    script:
        - cd html
        - npm run test -- --coverage
    artifacts:
        name: "client-coverage-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - html/coverage/
    cache:
        key: npm
        paths:
            - html/node_modules/

test:server:
    stage: test
    image: $DOCKER_ASPNETCORE_IMAGE
    dependencies:
        - build:server
    needs:
        - build:server
    script:
        - dotnet test --collect:"XPlat Code Coverage"
    artifacts:
      name: "server-coverage-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
      paths:
        - server/tests/MythicTable.Integration.Tests/TestResults/*
        - server/tests/MythicTable.Tests/TestResults/*

upload:coverage:
    stage: package
    dependencies:
        - test:server
        - test:client
    needs:
        - test:server
        - test:client
    script:
        - bash <(curl -s https://codecov.io/bash) -t $CODECOV_TOKEN

test:k8syaml:
    stage: test
    image: dtzar/helm-kubectl
    environment:
        name: edge
        url: https://edge.mythictable.com
    script:
        - kubectl config set-cluster mythic-table-edge-api --server=$KUBE_URL
        - sed -i "s/<VERSION>/latest/g" deployment.yaml
        - sed -i "s/<CI_ENVIRONMENT_SLUG>/$CI_ENVIRONMENT_SLUG/g" deployment.yaml
        - sed -i "s/<CI_PROJECT_PATH_SLUG>/$CI_PROJECT_PATH_SLUG/g" deployment.yaml
        - sed -i "s/<CI_DOMAIN>/edge.mythictable.com/g" deployment.yaml
        - sed -i "s/<CI_SCOPE>/edge/g" deployment.yaml
        - kubectl --token=$KUBE_TOKEN apply -f deployment.yaml --dry-run=server
    
package:
    image: $DOCKER_ASPNETCORE_IMAGE
    stage: package
    except:
        - releases/firstplayable
    dependencies:
        - build:server
        - build:client
    needs:
        - build:server
        - build:client
        - test:client
        - test:server
        - test:k8syaml
    script:
        - dotnet publish -c Release --packages .nuget -o ./package server/src/MythicTable/
        - dotnet publish -c Release --packages .nuget -o ./migrator server/src/Migrator/
        - mkdir -p ./package/wwwroot
        - cp --archive ./html/dist/* ./package/wwwroot/
    artifacts:
        name: "mythictable-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - package/
            - migrator/
    cache:
        key: nuget
        paths:
            - .nuget/

containerize:
    stage: containerize
    except:
        - releases/firstplayable
    image: docker:stable
    services:
        - docker:dind
    dependencies: 
        - package
    needs:
        - package
    script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker pull $CONTAINER_IMAGE:latest || echo "Ignoring missing container"
        - "docker build -f Dockerfile.gitlab-ci -t $CONTAINER_IMAGE:$CONTAINER_TAG -t $CONTAINER_IMAGE:latest ."
        - "docker push $CONTAINER_IMAGE:$CONTAINER_TAG"
        - "docker push $CONTAINER_IMAGE:latest"

containerize-migrator:
    stage: containerize
    only:
        - main
    image: docker:stable
    services:
        - docker:dind
    script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker pull $MIGRATOR_CONTAINER_IMAGE:latest || echo "Ignoring missing container"
        - docker build -f server/src/Migrator/Dockerfile -t $MIGRATOR_CONTAINER_IMAGE:$CONTAINER_TAG -t $MIGRATOR_CONTAINER_IMAGE:latest .
        - docker push $MIGRATOR_CONTAINER_IMAGE:$CONTAINER_TAG
        - docker push $MIGRATOR_CONTAINER_IMAGE:latest

deploy:production:
    stage: deploy
    only:
        - main
    dependencies:
        - containerize
    needs:
        - containerize
    image: dtzar/helm-kubectl
    environment:
        name: edge
        url: https://edge.mythictable.com
    script:
        - kubectl config set-cluster mythic-table-edge-api --server=$KUBE_URL
        - sed -i "s/<VERSION>/$CONTAINER_TAG/g" deployment.yaml
        - sed -i "s/<CI_ENVIRONMENT_SLUG>/$CI_ENVIRONMENT_SLUG/g" deployment.yaml
        - sed -i "s/<CI_PROJECT_PATH_SLUG>/$CI_PROJECT_PATH_SLUG/g" deployment.yaml
        - sed -i "s/<KEY_DOMAIN>/key.mythictable.com/g" deployment.yaml
        - sed -i "s/<CI_DOMAIN>/edge.mythictable.com/g" deployment.yaml
        - sed -i "s/<CI_SCOPE>/edge/g" deployment.yaml
        - kubectl --token=$KUBE_TOKEN apply -f deployment.yaml


build:client:fp:
    image: node:lts-alpine
    stage: build
    only:
        - releases/firstplayable
    script:
        - cd html
        - npm install
        - npm run build -- --mode firstplayable
    artifacts:
        name: "client-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - html/dist/
        expire_in: 1h
    cache:
        key: "npm-2020-11-07"
        paths:
            - html/node_modules/

package:fp:
    image: $DOCKER_ASPNETCORE_IMAGE
    stage: package
    only:
        - releases/firstplayable
    dependencies:
        - build:server
        - build:client:fp
    script:
        - dotnet publish -c Release --packages .nuget -o ./package server/src/MythicTable/
        - mkdir -p ./package/wwwroot
        - cp --archive ./html/dist/* ./package/wwwroot/
    artifacts:
        name: "mythictable-$CI_COMMIT_REF_SLUG-$CI_COMMIT_SHORT_SHA"
        paths:
            - package/
    cache:
        key: nuget
        paths:
            - .nuget/

containerize:fp:
    stage: containerize
    only:
        - releases/firstplayable
    image: docker:stable
    services:
        - docker:dind
    dependencies: 
        - package:fp
    script:
        - docker info
        - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
        - docker pull $CONTAINER_IMAGE:latest || echo "Ignoring missing container"
        - "docker build -f Dockerfile.gitlab-ci -t $CONTAINER_IMAGE:$CONTAINER_TAG -t $CONTAINER_IMAGE:latest ."
        - "docker push $CONTAINER_IMAGE:$CONTAINER_TAG"
        - "docker push $CONTAINER_IMAGE:latest"

deploy:fp:
    stage: deploy
    only:
        - releases/firstplayable
    image: dtzar/helm-kubectl
    environment:
        name: fp
        url: https://fp.mythictable.com
    script:
        - kubectl config set-cluster mythic-table-fp --server=$KUBE_URL
        - sed -i "s/<VERSION>/$CONTAINER_TAG/g" deployment.yaml
        - sed -i "s/<CI_ENVIRONMENT_SLUG>/$CI_ENVIRONMENT_SLUG/g" deployment.yaml
        - sed -i "s/<CI_PROJECT_PATH_SLUG>/$CI_PROJECT_PATH_SLUG/g" deployment.yaml
        - sed -i "s/<KEY_DOMAIN>/key.mythictable.com/g" deployment.yaml
        - sed -i "s/<CI_DOMAIN>/fp.mythictable.com/g" deployment.yaml
        - sed -i "s/<CI_SCOPE>/fp/g" deployment.yaml
        - kubectl --token=$KUBE_TOKEN apply -f deployment.yaml

include:
  - template: Container-Scanning.gitlab-ci.yml

container_scanning:
    stage: container_scanning
    needs: ["containerize"]
    variables:
        GIT_STRATEGY: fetch
        DOCKERFILE_PATH: "Dockerfile.gitlab-ci"
