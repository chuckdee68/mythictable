<!-- Title suggestion: [Onboarding] Newcomer's name -->

# Welcome to Mythic Table's QA Team!
We hope you're as excited as we are to be part of this project! The QA Process is VERY important to us. Please keep in mind that we are all learning, so please ask as many questions as you like!

Please go through the items below and check them off as they are completed. Yes, there is a lot here. If you are proficient with the systems being used you should be able to get through it very quickly. If you are not, then please take the time to absorb as much as you can. Take the time you need, and ask questions in slack (#mythic-table or DM Carl) where needed. The better understanding and buy-in you have to this process, the more you can contribute to the team:
<br/><br/>
<hr/>

# **Get them OnBoard!**
To be completed by MythicTeam member
- [ ] Get email address or existing Slack/GitLab user information
- [ ] [Invite new member to GitLab via email](https://gitlab.com/groups/mythicteam/-/group_members)
  - Add as Developer role
- [ ] Invite new member to Slack via email
  - Invite as member
<!--Reference image on where to invite from inside of Slack ![](https://i.imgur.com/zOLr1pS.png)-->
<br/>
<hr/>

# **Learn Mythic Table**

## 1. Please start out by becoming familiar with the following:
Bookmarking is recommended

### Required 
- [ ] [*Main Site*](https://www.mythictable.com/)
- [ ] [*Non-profit Announcement*](https://www.mythictable.com/org/now-non-profit)
- [ ] [*Mission Statement*](https://www.mythictable.com/mission)
- [ ] [*Features*](https://www.mythictable.com/features)
- [ ] [*Gitlab*](https://gitlab.com/mythicteam/mythictable) 
  - [ ] [*Wiki*](https://gitlab.com/mythicteam/mythictable/-/wikis/home)
  - [ ] [*A Note on Acceptance Criteria*](https://gitlab.com/mythicteam/mythictable/-/wikis/qa/acceptance-criteria)


### Optional
- [ ] [*Mythic Table Dev Stories*](https://www.mythictable.com/team/mythicstories/index/)

## 2. Process
Mythic Table doesn't have a lot of process, but what we have we've adopted for very good reasons.  Learn about our process here.

- [ ] [Introduction](https://gitlab.com/mythicteam/mythictable/-/wikis/Working-on-Mythic-Table)
- [ ] [Guidelines](https://gitlab.com/mythicteam/mythictable/-/wikis/workflow-guidelines)
- [ ] [Milestones](https://gitlab.com/mythicteam/mythictable/-/milestones)
- [ ] [Kanban Board](https://gitlab.com/mythicteam/mythictable/-/boards)
  - [ ] Optional: ![](https://i.imgur.com/TtIyDnO.png "Youtube")[Using Issue Boards as Kanban Boards in GitLab](https://youtu.be/CiolDtBIOA0?t=115)
- [ ] [**Reviewing an Issue**](https://gitlab.com/mythicteam/mythictable/-/wikis/qa/review-an-issue)
<br/><br/>
<hr/>

# **Get Social**

## 3. Please like as many of our social media accounts as you can:
I mean, hopefully you're proud enough of us that you would want to!

- [ ] ![alt text](https://i.imgur.com/TtIyDnO.png "Youtube") - [**Youtube**](https://www.youtube.com/mythictable)
- [ ] ![alt text](https://i.imgur.com/RBSCNgo.png "Facebook") - [**Facebook**](https://www.facebook.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/M9RR4Hf.png "Instagram") - [**Instagram**](https://www.instagram.com/mythic_table/)
- [ ] ![alt text](https://i.imgur.com/hgpY9gu.png "Reddit") - [**Reddit**](https://www.reddit.com/r/mythictable/)
- [ ] ![alt text](https://i.imgur.com/rEAMp9o.png "Twitter") - [**Twitter**](https://twitter.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/kX34PqK.png "Patreon") - [**Patreon**](https://www.patreon.com/mythictable/)
- [ ] ![alt text](https://i.imgur.com/m9k88St.png "Tumblr") - [**Tumblr**](https://mythictable.tumblr.com/)
- [ ] ![alt text](https://i.imgur.com/EbNeByg.png "Product Hunt") - [**Product Hunt**](https://www.producthunt.com/posts/mythic-table/)

## 4. Tell the world!
- [ ] **Join us on Linkedin** https://www.linkedin.com/company/mythic-table/
<br/><br/>
<hr/>

# **Get Connected**
## **5. Slack**
We use Slack for almost all of our communication. Being active there is critical to being and effective team member. You should have already received an invite from a MythicTeam member. If you have not, please reach out to the member who is working with getting you started with the team. 

### 5.1 Join the following channels on Slack:
- [ ] #random - Better than most RNGeesus.
- [ ] #qa - We will talk QA here.
- [ ] #mythic-table - Talk with the team about what you're working on. A good place for general help and asking people to help with Review of tickets. 
- [ ] #gitlab - You will get updates from GitLab, including tickets being moved to Review
- [ ] Reply to or start a **Thread**
- [ ] Send Marc and Carl a private message
- [ ] Customize your notifications

### 5.2 Join other channels
Other useful slack channels. It is optional to join these:
* [ ] #daily - see what the team is up to. You can contribute to this by adding the DailyBot app to slack.
* [ ] #gitlab - You will get updates from GitLab, including tickets being moved to Review
* [ ] #community
* [ ] #community-outreach
* [ ] #content
* [ ] #design
* [ ] #dev (It is for Programmers/Developers but it is good to listen in on)
* [ ] **Join #coffee-break**! We use this for our donut channel social meetups.  Get to know the rest of the team!

### 5.3 Optional
- [ ] Get Slack on your phone
- [ ] Add DailyBot app and set it up
- [ ] Browse and add other channels 

# **Get Productive!**
## 6. GitLab
At this point, you should already be a part of the GitLab team... I mean... how else would you be reading this, right? As a QA team member, your interaction with Git will be pretty limited relatively speaking. Below are some common steps you will be taking in GitLab. They are designed to get you familiar with working inside of GitLab at the capacity that you will predominantly be working at. If there is a step which you are not familair with, ask Carl, Marc, or anyone in #mythic-table for assistance. 

### 6.1 Issuing a ticket
<!-- Update this when how-to/Issuing a ticket is complete -->
- [ ] Create a new issue [(how-to/make issues)](https://gitlab.com/mythicteam/mythictable/-/wikis/how-to/make-issues)
  - [ ] Template: Story
  - [ ] Title: [Story] < my-name > onboarding
  - [ ] User Story: "As a new MythicTeam QA Team Member, I can create issues with meaningful Acceptance Criteria and review issues properly"
  - [ ] Details: Have fun here. Describe your expectations as you go through this process. Typically a dev team member will create the description so you won't typically be providing this information. 
  - [ ] Steps: "- [ ] Typically the devs will fill this in, but I will just put that I'm following the onboarding procedures here."
- [ ] Leave the default label as is, but also include the "Onboarding" label
- [ ] Change the weight to 1
- [ ] Preview the ticket to ensure everything is correct and looks pretty
- [ ] Click "Assign to me" to set yourself as the assigned team member
- [ ] Click "Submit issue"

Congrats, you've created your first issue as a MythicTeam QA Team member!

### 6.2 The Life Cycle of an Issue
Now that you can create a ticket let's talk about our main purpose here at MythicTable. We need helpers to connect the developers with the clients. We do that by helping create Acceptance Criteria which passes the client's expectations along to the developers to make functional features. This starts as early as the developers ask for Acceptance Criteria, but no later than ~"phase::planning".
- [ ] Find your ticket on the issue board. You may need to switch to the Development board
- [ ] Drag your ticket from the Open column to the ~"phase::planning" column. 

This is the same as adding the ~"phase::planning" label to your ticket. You can manually do this from the issue page. The ~"phase::planning" phase is where Acceptance Criteria will normally be added. Most Issues will not have Acceptance Criteria. If you see one without criteria then add the ~"Acceptance Needed" label
- [ ] Click your issue, anywhere except on the title.
- [ ] In the right sidebar find the Labels section, and click edit.
- [ ] Add the ~"Acceptance Needed" label if it's not already there. 

This tells the devs they need to add Acceptance Criteria before the issue is moved into the ~"phase::active" column. 

Sometimes the devs will ask for help developing good acceptance criteria, and that is our responsibility. Let's add some AC to our ticket. 
- [ ] Click the title of our issue. It should be a link to the Issue page.
- [ ] Edit the issue by clicking the pencil icon in the upper right next to the title.
- [ ] Review [Acceptance Criteria](https://gitlab.com/mythicteam/mythictable/-/wikis/qa/acceptance-criteria)
- [ ] Read the Acceptance Criteria section, including the part in the comment tags. Based on this come up with at least one good example criteria for your User Story issue.
- [ ] Include "A MythicTeam member other than myself can find this criteria in my issue and check it as complete. They are able to find it because I asked in #mythic-table for someone to review my ticket." as an Acceptance Criteria
- [ ] Remove the ~"Acceptance Needed" label
- [ ] Click Save changes

We now have Acceptance Criteria in our issue. Congrats! This means the dev team can now move forward with the work for this ticket. Normally they will move the issue to the ~"phase::active" column in the Issues Board, but we can do it ourselves this time. If you would like to review some hints on creating good Acceptance Material, be sure to check out [Some notes on Acceptance criteria (AC)](https://gitlab.com/mythicteam/mythictable/-/wikis/qa/acceptance-criteria)

- [ ] Move your issue from ~"phase::planning" to ~"phase::active"

Developers will work on the issue at this point. You may see it the next day. You may see it the next month. Eventually they will all have to come to the ~"phase::review" column. This is where we will do more work.

- [ ] Move your issue from ~"phase::active" to ~"phase::review"

With the exception of the last two steps we will go over next, this is the full Life Cycle of an Issue. 

### 6.3 Review a ticket
Developers should not be the ones reviewing their tickets. There are multiple ways for devs to alert the MythicTeam that an issue is ready for review. The most obvious is the one we just did: moving the issue to the ~"phase::review" column. We can regularly check the Issue Boards often for Issues in both Development and Bugs. We can also check #qa for ~"phase::review" requests. We recommend these be the first task you do when you check in to work on MythicTable (did we mention how much we appreciate the time you are spending with us? Sincerely, Thanks!) 

*Developers cannot and should not pass Acceptance Criteria on a ticket which they have contributed code to.* If you contribute to a ticket, let someone else review it. Typically as a QA Team member, you will not have contributed to the code development of a ticket, but if you have ask someone eles to review it for you. Since we have not contributed to any code on our ticket, I think the Langoliers will be ok with us reviewing our own ticket this time. 
- [ ] Review [review a ticket](https://gitlab.com/mythicteam/mythictable/-/wikis/qa/review-an-issue)
- [ ] Open our issue page
- [ ] Begin checking to see if the Acceptance Criteria passes or fails. If it passes, check it as completed.

Oh, did you notice that there's an Acceptance Criteria which we can't pass by ourselves. What will we do? 
- [ ] Post a request in #mythic-table or #qa for someone to review your ticket. Copy the link to the issue and paste it into the channel. Be polite, you can't finish until someone clicks that box for you.

Once all the criteria is checked as passed, you are done! Congratulate yourself in #mythic-table if you'd like since that's what [The Review Process](https://gitlab.com/mythicteam/mythictable/-/wikis/qa/review-an-issue) calls for. 
- [ ] Move the issue to the Closed column

That's it, you've just cone everything we really need you do for us! CONGRATS!

### 6.3 Optional (maybe while waiting for others to mark your AC as complete)
- [ ] [Become familiar with the full Git system if you would like to pull a branch for your own development or use an IDE on your own system.](https://try.github.io/)
- [ ] [Some quick tips on writing killer Acceptance Criteria](https://rubygarage.org/blog/clear-acceptance-criteria-and-why-its-important)
- [ ] [Acceptance Crtieria Examples](https://agileforgrowth.com/blog/acceptance-criteria-checklist/)

<hr/>

Thanks for reading this ticket! If you notice something that should be added please let me know.
*Carl Gibson, QA Manager
Mythic Table Foundation*
****

/label ~"type::onboarding"
